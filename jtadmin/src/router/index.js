import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
import ElementUI from '../components/ElementUI.vue'
import Home from '../components/Home.vue'
import User from '../components/user/user.vue'
import Item from '../components/items/Item.vue'
import ItemCat from '../components/items/ItemCat.vue'
import AddItem from '../components/items/addItem.vue'

//使用路由机制
Vue.use(VueRouter)
const routes = [
  {path: '/', redirect: '/login'},
  {path: '/login', component: Login},//访问/login----打开Login组件
  {path: '/elementUI', component: ElementUI},//访问/elementUI----打开ElementUI组件
  {path: '/home', component: Home,children:[
    {path: '/user', component: User},
    {path: '/item', component: Item},
    {path: '/itemCat', component: ItemCat},
    {path: '/item/addItem', component: AddItem}
  ]}//访问/home----打开Home组件----进入主页
]

//路由导航守卫!!!!!!!

const router = new VueRouter({
  routes
})

export default router
