package com.pm.controller;

import com.pm.pojo.Rights;
import com.pm.service.RightsService;
import com.pm.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/rights")
public class RightsController {
    @Autowired
    private RightsService rightsService;
    @GetMapping("/getRightsList")
    public SysResult getRightsList() {
        List<Rights> list = rightsService.getRightsList();
        return SysResult.success(list);
    }

}
