package com.pm.controller;

import com.pm.pojo.User;
import com.pm.service.UserService;
import com.pm.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {
    @Autowired
    private UserService userService;
    @PostMapping("/login")
    //POST请求传入的数据类型是json串的形式，请求的数据是被封装在user里面的，传入user对象，用注解来解析json字符串
    public SysResult login(@RequestBody User user){
        //业务要求，返回给用户一个密钥，写为一个字符串类型，默认值为null，从下一层获取数据
        String token=userService.login(user);
        //获取数据有两种情况，第一种是业务执行成功，返回执行成功的方法，第二种返回失败，即无法从下一层拿到数据，默认值则为null
        //判断是否能拿到数据，拿不到数据就返回失败的方法，用来截取设置的默认返回结果
        if (token ==null){
            return SysResult.fail();
        }
        //设置默认返回一个成功的方法
        return SysResult.success(token);
    }
}
