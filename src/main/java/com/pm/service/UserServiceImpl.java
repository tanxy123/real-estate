package com.pm.service;

import com.pm.dao.UserMapper;
import com.pm.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.UUID;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserMapper userMapper;
    @Override
    public String login(User user) {
        //业务层要求处理用户传入的数据，比如密码传入的是明文，但是数据库是密文，常用的加密手段是sha1/md5算法/md5hash算法,因此需要加密处理
        //md5和sha1都有破解，md5hash算法更复杂更安全,因此加密过程如下
        String password=user.getPassword();
        byte[] bytes=password.getBytes();
        String md5hash= DigestUtils.md5DigestAsHex(bytes);
        //根据传入的用户信息查询数据库
        //有结果： 返回token 但是保证每个人数据唯一，因此用到UUID
        //没有结果或者异常： 返回一个空结果
        user.setPassword(md5hash);
        User userLogin=userMapper.findUserByUP(user);
        //将获取的对象进行判断，是否存在，不存在返回空，存在则执行uuid
        if (userLogin==null) {
            return null;
        }
        //密钥要求是唯一的，所以是用UUID的方法
        String token= UUID.randomUUID().toString();
        return token;
    }
}
