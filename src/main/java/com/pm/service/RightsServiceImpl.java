package com.pm.service;

import com.pm.dao.RightsMapper;
import com.pm.pojo.Rights;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RightsServiceImpl implements RightsService{
    @Autowired
    private RightsMapper rightsMapper;
    @Override
    public List<Rights> getRightsList() {
        List<Rights> rightsList = rightsMapper.getRightsList();
        return rightsList;
    }
}
