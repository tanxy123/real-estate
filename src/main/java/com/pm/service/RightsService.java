package com.pm.service;

import com.pm.pojo.Rights;

import java.util.List;

public interface RightsService {
    List<Rights> getRightsList();
}
