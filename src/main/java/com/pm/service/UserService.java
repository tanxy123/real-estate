package com.pm.service;

import com.pm.pojo.User;

public interface UserService {
    String login(User user);
}
