package com.pm.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pm.pojo.User;

public interface UserMapper extends BaseMapper<User> {
    User findUserByUP(User user);
}
