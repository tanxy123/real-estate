package com.pm.dao;

import com.pm.pojo.Rights;

import java.util.List;

public interface RightsMapper {
    List<Rights> getRightsList();
}
