package com.pm.aop;

import com.pm.vo.SysResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//用于全局异常控制处理
//@Aspect
//@RestControllerAdvice//advice通知！
//全局异常处理机制，只拦截Controller层
public class SystemException {

//    @ExceptionHandler(RuntimeException.class)//相当于指定异常的类型，进行拦截(运行时异常)

    public SysResult exception(Exception e) {
        e.printStackTrace();//控制台打印异常
        return SysResult.fail();//201
    }
}
