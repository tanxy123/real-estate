package com.pm.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration//标识我是个配置类
public class MybatisPlusConfig {
//    @Bean//将自定义的对象交给spring容器管理
    public MybatisPlusInterceptor mybatisPlusInterceptor(){
        //1.定义一共拦截器
        MybatisPlusInterceptor interceptor=new MybatisPlusInterceptor();
        //2.调用拦截器的addInnerInterceptor()方法，参数是固定API，标识用的是什么数据库
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }
}
