package com.pm.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain=true)
public class BasePojo implements Serializable {
    /**在公共的pojo类里面添加自动填充项*/
    @TableField(fill = FieldFill.INSERT)//表示新增的时候对该属性进行自动填充
    private Date created;	//表示入库时需要赋值
    @TableField(fill = FieldFill.INSERT_UPDATE)//表示修改的时候对该属性进行自动填充
    private Date updated;	//表示入库/更新时赋值.
}
