package com.pm.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("user")//让对象与表名一一映射
public class User extends BasePojo {
    @TableId(type = IdType.AUTO)//表示与表中主键映射,并且主键自增
    private Integer id;
    @TableField("username")//表示与字段进行映射，如果字段名字和属性的名字一样（含驼峰规则），则可以省略注解，
    private String username;
    private String password;
    private String phone;
    private String email;
    private Boolean status;     //true 正常 false 停用,但是默认值是null
    private String groupId;
}

