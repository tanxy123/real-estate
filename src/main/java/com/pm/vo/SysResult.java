package com.pm.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class SysResult implements Serializable {
    private Integer status;//200表示成功，201表示异常
    private String msg;//提示信息
    private Object data;//系统返回值
    public static SysResult fail(){
        return new SysResult(201,"业务调用失败",null);
    }
    //根据用户的不同提交需求，反馈给用户不同的展示信息，需要重载方法：
    public static SysResult success(){
        return new SysResult(200,"业务执行成功",null);
    }
    public static SysResult success(Object data){
        return new SysResult(200,"业务执行成功",data);
    }
    public static SysResult success(String msg,Object data){
        return new SysResult(200,msg,data);
    }
}
